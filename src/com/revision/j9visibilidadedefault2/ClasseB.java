package com.revision.j9visibilidadedefault2;

import com.revision.j9visibilidadedefault.ClasseA;

public class ClasseB extends ClasseA {
	
	private String str = "";

	public ClasseB() {
		// this.str = super.string; // N�o � poss�vel acessar 
	}

	public String getStr() {
		return str;
	}
	
}

/**
 * Quando nenhum nodificador � informado, a visibilidade � default, onde somente classes no mesmo pacote podem acessar.
 * 
 * Classes de outros pacotes, como neste exemplo, que esta ClasseB herda da ClasseA em outro pacote, n�o � poss�vel acessar.
 * 
 */
