package com.revision.j4operadores;

public class OperadorTernario {

	public static void main(String[] args) {
		
		int valor = 7;
		
		System.out.println(valor % 2 ==  0 ? "par" : "impar");
		
	}
}
