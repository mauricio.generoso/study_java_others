package com.revision.j7dadosmaquina;

import java.util.Enumeration;

/*
 * Documenta��o: https://docs.oracle.com/javase/7/docs/api/java/lang/System.html#getProperties%28%29
 */
public class DadoMaquina {
	
	public static void main(String[] args) {
		
		System.out.println("Coisas do Java");
		System.out.println();
		System.out.println("Java Runtime Environment version:               " + System.getProperty("java.version"));
		System.out.println("Java Runtime Environment vendor:                " + System.getProperty("java.vendor"));
		System.out.println("Java vendor URL:                                " + System.getProperty("java.vendor.url"));
		System.out.println("Java installation directory                     " + System.getProperty("java.home"));
		System.out.println("Java Virtual Machine specification version:     " + System.getProperty("java.vm.specification.version"));
		System.out.println("Java Virtual Machine specification vendor:      " + System.getProperty("java.vm.specification.vendor"));
		System.out.println("Java Virtual Machine specification name:        " + System.getProperty("java.vm.specification.name"));
		System.out.println("Java Virtual Machine implementation version:    " + System.getProperty("java.vm.version"));
		System.out.println("Java Virtual Machine implementation vendor:     " + System.getProperty("java.vm.vendor"));
		System.out.println("Java Virtual Machine implementation name:       " + System.getProperty("java.vm.name"));
		System.out.println("Java Runtime Environment specification version: " + System.getProperty("java.specification.version"));
		System.out.println("Java Runtime Environment specification vendor:  " + System.getProperty("java.specification.vendor"));
		System.out.println("Java Runtime Environment specification name:    " + System.getProperty("java.specification.name"));
		System.out.println("Java class format version number:               " + System.getProperty("java.class.version"));
		System.out.println("Java class path:                                " + System.getProperty("java.class.path"));
		System.out.println("List of paths to search when loading libraries: " + System.getProperty("java.library.path"));
		System.out.println("Default temp file path:                         " + System.getProperty("java.io.tmpdir"));
		System.out.println("Name of JIT compiler to use:                    " + System.getProperty("java.compiler"));
		System.out.println("Path of extension directory or directories:     " + System.getProperty("java..ext.dirs"));
		
		System.out.println();
		System.out.println("Coisas da m�quina");
		System.out.println();
		System.out.println("Mem�ria livre: " + Runtime.getRuntime().freeMemory());
		System.out.println("Mem�ria m�xima: " + Runtime.getRuntime().maxMemory());
		System.out.println("Mem�ria Total: " + Runtime.getRuntime().totalMemory());
		System.out.println("Processdores dispon�veis: " + Runtime.getRuntime().availableProcessors());
		System.out.println();
		System.out.println("os.name:        " + System.getProperty("os.name"));
		System.out.println("os.arch:        " + System.getProperty("os.arch"));
		System.out.println("os.version:     " + System.getProperty("os.version"));
		System.out.println("file.separator: " + System.getProperty("file.separator"));
		System.out.println("path.separator: " + System.getProperty("path.separator"));
		System.out.println("line.separator: " + System.getProperty("line.separator"));
		System.out.println("user.name:      " + System.getProperty("user.name"));
		System.out.println("user.home:      " + System.getProperty("user.home"));
		System.out.println("user.dir:       " + System.getProperty("user.dir"));
		
		System.out.println();
		System.out.println("Outros dados: ");
		System.out.println();
		Enumeration liste = System.getProperties().propertyNames();
	    String cle;
	    while( liste.hasMoreElements() ) {
	        cle = (String)liste.nextElement();
	        System.out.println( cle + " = " + System.getProperty(cle) );
	    } 
	}
	
}
