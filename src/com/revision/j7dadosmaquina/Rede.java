package com.revision.j7dadosmaquina;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;

public class Rede {
	
	public static void main(String[] args) {
		try {
			InetAddress Ip = InetAddress.getLocalHost();
			System.out.println("IP: " + Ip.getHostAddress());
			System.out.println("HostName: " + Ip.getCanonicalHostName());
			System.out.println("---------------------------------------------");
			System.out.println("Lists all the network interfaces and their addresses on a machine:");
			getInfos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static void getInfos() throws SocketException {
		Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
		for (NetworkInterface netint : Collections.list(nets))
			displayInterfaceInformation(netint);
	}

	static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {
		System.out.printf("Display name: %s\n", netint.getDisplayName());
		System.out.printf("Name: %s\n", netint.getName());
		byte[] mac = netint.getHardwareAddress(); // a byte array containing the address (usually MAC) or null
		Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
		for (InetAddress inetAddress : Collections.list(inetAddresses)) {
			System.out.printf("InetAddress: %s\n", inetAddress);
		}
		System.out.printf("\n");
	}
}