package com.revision.j8modificadorprotected2;

public class ClasseTeste {
	
	public static void main(String[] args) {
		ClasseB b = new ClasseB("");
		System.out.println(b.getStr());
	}
}

/**
 * O modificador de acesso protected tem visibilidade em subClasses e em n�vel de pacote.
 * 
 * 
 * Neste exemplo existe no pacote j8modificadorprotected a ClasseA com a vari�vel string protected
 * 
 * No pacote j8modificadorprotected2 existe a classeB que herda de ClasseA, com a vari�vel str que recebe a vari�vel string da superclasse. 
 * 
 * Nessa classe ClasseTeste, n�o � poss�vel acessar a vari�vel string da ClasseA porque est� em pacote diferente. 
 *
 * Verificar que com uma classe igual a esta no mesmo pacote da ClasseA, � poss�vel acessar a vari�vel protected devido ao n�vel de pacote. 
 * 
 */



