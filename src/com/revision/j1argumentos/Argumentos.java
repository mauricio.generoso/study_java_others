package com.revision.j1argumentos;

/**
 * Para executar com argumentos, deve-se inicializar pela linha de comando.
 * Deve compilar o arquivo. "javac Argumentos.java"
 * Deve executar o arquivo compilado. "java Argumentos"
 * 
 * Deve passar com espa�o o argumento, por exempli: "java Argumentos Ol�"
 * 
 * @author Mauricio
 *
 */
public class Argumentos {
	
	public static void main(System[] args){
		System.out.println("Voc� digitou : " + args[0]);
	}
}
