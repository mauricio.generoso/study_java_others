package com.revision.j12BitInteger;

import java.math.BigInteger;

public class Fatorial {
	
	public static void main(String[] args) {
		System.out.println(Fatorial.recursiveFatorial(500));
	}
	
	private static BigInteger recursiveFatorial	(int n) {
		if (n == 0) return BigInteger.ONE;
		return BigInteger.valueOf(n).multiply(recursiveFatorial(n - 1));
	}
	
}
