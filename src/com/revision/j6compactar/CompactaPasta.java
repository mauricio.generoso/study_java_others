package com.revision.j6compactar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CompactaPasta {

	public static void main(String[] args) {
		try {
		    FileOutputStream fos = new FileOutputStream("pasta.zip");
		    ZipOutputStream zipOut = new ZipOutputStream( fos );
		     
		    File pasta = new File("pasta");
		    for(File arq : pasta.listFiles() ){
		        zipOut.putNextEntry( new ZipEntry( arq.getName().toString() ) );
		                 
		        FileInputStream fis = new FileInputStream( arq );
		                 
		        int content;
		        while ((content = fis.read()) != -1) {
		            zipOut.write( content );
		        }
		                 
		        zipOut.closeEntry();
		                
		    }
		         
		    zipOut.close();
		         
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
}
