package com.revision.j6compactar;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CompactarDoHD {

	public static void main(String[] args) {
		try {
		    FileInputStream fis = new FileInputStream("arquivo.txt");
		     
		    FileOutputStream fos = new FileOutputStream("arquivo.zip");
		    ZipOutputStream zipOut = new ZipOutputStream( fos );
		 
		    zipOut.putNextEntry( new ZipEntry("arquivo.txt") );
		             
		    int content;
		    while ((content = fis.read()) != -1) {
		        zipOut.write(content );
		    }
		             
		    zipOut.closeEntry();
		    zipOut.close();
		                         
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
}
