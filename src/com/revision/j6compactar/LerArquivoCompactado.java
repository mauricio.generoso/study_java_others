package com.revision.j6compactar;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class LerArquivoCompactado {

	public static void main(String[] args) {
		try {
		    FileInputStream fis = new FileInputStream("arquivo.zip");
		             
		    ZipInputStream zipIn = new ZipInputStream( fis );
		 
		    ZipEntry entry = zipIn.getNextEntry();
		             
		    while (zipIn.available() > 0){
		        System.out.print( (char) zipIn.read() );
		    }                   
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
}
