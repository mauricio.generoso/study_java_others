package com.revision.j6compactar;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Compactar {

	public static void main(String[] args) {
		String texto = "Um texto qualquer vindo de algum lugar!" ;
	     
		try {
		    FileOutputStream fileOut = new FileOutputStream("texto.zip");
		    ZipOutputStream out = new ZipOutputStream(fileOut);
		             
		    out.putNextEntry( new ZipEntry("texto.txt") );
		    out.write( texto.getBytes() );
		    out.closeEntry();
		    out.close();
		             
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
}
