package com.revision.j5datas;

import java.util.Calendar;

public class TesteCalendar1 {

	public static void main(String[] args) {

		/**
		 * Essa classe produz valores de todos os campos do calend�rio para que possa
		 * ser formatada em determinadas l�nguas e estilos de calend�rios.
		 * 
		 * A classe Calendar � uma classe abstrada e n�o pode ser instanciada.
		 */
		
		System.out.println(Calendar.getInstance());
		System.out.println(Calendar.getInstance().getTime());

	}
}
