package com.revision.j5datas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarEDate {

	public static void main(String[] args) {
	
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yy");
		String dataRecebida = "23/11/2015";
		Curso novo = new Curso(); 
		Date dataFormatada = new Date();
		
		try {
			dataFormatada = formato.parse(dataRecebida);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		novo.setLancamento(dataFormatada);
		
		System.out.println("Data: " + dataFormatada);
		System.out.println("Data formatada: " + formato.format(dataFormatada));
		
		/*
		 * Converter para Calendar
		 */
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataFormatada);
		 
		System.out.println(formato.format(calendar.getTime()));
	}
	
}

class Curso {
	 
    private String nome;
    private Date dataDeLancamento;

    public void setLancamento(Date data) {
         this.dataDeLancamento = data;
    }
}
