package com.revision.j5datas;

import java.time.LocalDate;

public class Java8LocalDate {

	public static void main(String[] args) {
		LocalDate hoje = LocalDate.now();
		System.out.println(hoje); //2014-04-08 (formato ISO-8601)
	}
}
