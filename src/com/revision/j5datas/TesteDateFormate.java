package com.revision.j5datas;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class TesteDateFormate {

	public static void main(String[] args) {

		/**
		 * A classe DateFormat permite converter String em datas. Trata-se de uma classe
		 * abstrada e n�o � poss�vel instanci�-la.
		 */

		Calendar c = Calendar.getInstance();
		c.set(2013, Calendar.FEBRUARY, 28);
		Date data = c.getTime();
		System.out.println("Data atual sem formata��o: " + data);

		// Formata a data
		DateFormat formataData = DateFormat.getDateInstance();
		System.out.println("Data atual com formata��o: " + formataData.format(data));

		// Formata Hora
		DateFormat hora = DateFormat.getTimeInstance();
		System.out.println("Hora formatada: " + hora.format(data));

		// Formata Data e Hora
		DateFormat dtHora = DateFormat.getDateTimeInstance();
		System.out.println(dtHora.format(data));

		System.out.println("============================");

		Calendar c2 = Calendar.getInstance();
		Date data2 = c2.getTime();

		DateFormat f = DateFormat.getDateInstance(DateFormat.FULL); // Data COmpleta
		System.out.println("Data brasileira: " + f.format(data2));

		f = DateFormat.getDateInstance(DateFormat.LONG);
		System.out.println("Data sem o dia descrito: " + f.format(data2));

		f = DateFormat.getDateInstance(DateFormat.MEDIUM);
		System.out.println("Data resumida 1: " + f.format(data2));

		f = DateFormat.getDateInstance(DateFormat.SHORT);
		System.out.println("Data resumida 2: " + f.format(data2));
	}
}
