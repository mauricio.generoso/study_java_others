package com.revision.j5datas;

public class Resumo {
}

/*
 * No Java 1.0 havia somente a classe Date, que � um tanto quanto dif�cil de se trabalhar para manipular datas.
 * 
 * No Java 1.1 introduziu-se a classe Calendar, que melhorou, possibilitou a internacionaliza��o mas ainda custava muito trabalho
 * para manipular datas.
 * 
 * Como alterantiva era-se usado a biblioteca JodaTime.
 *  
 * A classe SimpleDateFormat � usada para formatar as datas.
 * A classe DateFormat � usada tamb�m para formatar datas.
 * -----------------------------------------------------------------------------------------
 * 
 * JAVA 8
 * 
 * No java 8 foi criado o pacote java.time com novas classes para manipular datas. 
 * 
 * At� ent�o no java a data era contada com um long a partir de 01/01/1970, em milisegundos.
 * 
 * A partir do Java 8 a classe Instant � utilizada para representar esse n�mero da data atual, por�m com precis�o em nanosegundos.
 * 
 * � poss�vel utilizar as classes Instant e Duration para calcular o tempo de um algoritmo.
 * 
 * No Java 8 as datas s�o trabalhadas de duas formas, conforme entende-se pela m�quina, e conforme entende-se pelo humano. 
 * 
 * A classe Instant trabalha na representa��o para computador, contanto a partir de 01/01/1970.
 * As classes LocalDat, LocalTime e LocalDateTime trabalham com representa��o para humanos para datas e horas.
 * 
 * Para trabalhar com Fuso hor�rio deve-se utilizar ZonedDateTime.
 * 
 * A classe Calendar foi adaptada para possibilidar trabalhar com Java 8:
 * 
 * Calendar calendario =  
      new Calendar.Builder()  
        .setDate(2014, Calendar.APRIL, 8)  
        .setTimeOfDay(10, 2, 57)  
        .setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"))  
        .setLocale(new Locale("pt", "br"))
        .build(); 
 *
 * 
 * Obs: Existem outras classes ainda, como MonthDay, DateTimeFormatter.
 * 
 * Obs: Todas as classes do pacote java.time s�o imut�veis, ou seja, n�o podem ser alteradas quando criado o objeto. S�o tamb�m thread-safe.
 * 
 */
