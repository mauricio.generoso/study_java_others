package com.revision.j5datas;

import java.time.Duration;
import java.time.Instant;

public class Java8Instant {

	public static void main(String[] args) {
		
		Instant agora = Instant.now();
		System.out.println(agora); //2014-04-08T10:02:52.036Z (formato ISO-8601)
		
		
		/*
		 * � poss�vel utilizar Instante para medir a dura��o de um algoritmo
		 */
		
		Instant inicio = Instant.now();
		//rodaAlgoritmo();
		try {
			Thread.sleep(3000l);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Instant fim = Instant.now();
		 
		Duration duracao = Duration.between(inicio, fim);
		long duracaoEmMilissegundos = duracao.toMillis(); // dura��o em milisegundos
		long duracaoEmsegundos = duracao.getSeconds(); // duta��o em segundos
		
		System.out.println("Millisegundos: " + duracaoEmMilissegundos);
		System.out.println("segundos: " + duracaoEmsegundos);
	}
}
