package com.revision.j5datas;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/*
 * A partir do Java 8, foi introduzida a classe LocalDate do pacote �java.time�, o qual possui uma melhor abstra��o no tratamento de datas.
 *
 * @author Mauricio
 *
 */
public class Java8Data {

	public static void main(String[] args) {

		DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate data = LocalDate.parse("23/11/2015", formato);
		System.out.println("Data: " + data);
		
		/*
		 * Para ter a formata��o necess�ria:
		 */
		System.out.println(formato.format(data));
	}
}
