package com.revision.j5datas;

import java.util.Date;

/**
 * No java a data e contada a partir de 01/01/1970 em milisegundos, que podem
 * ser obtidos com System.currentTimeMillis();
 * 
 * @author Mauricio
 *
 */
public class TesteDate1 {

	public static void main(String[] args) {
		
		System.out.println("Millisegundos atuais: " + System.currentTimeMillis());
	
		/*
		 * A classe Date j� � depreciada, n�o deve mais ser usada.
		 */
		
		Date date = new Date();
		System.out.println(date.toString());
		
	}
	
}
