package com.revision.j10jna;

import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;

public class Exemplo1 {

	public static void main(String[] args) {
		Kernel32 lib = (Kernel32) Native.loadLibrary("kernel32", Kernel32.class);
		System.out.println(lib.GetCurrentProcess());
		lib.Beep(500, 500);
		lib.Sleep(500);
		lib.Beep(698, 500);
		System.out.println(lib.GetCurrentProcess());
	}
	
	private interface Kernel32 extends StdCallLibrary {

		public boolean Beep(int FREQUENCY, int DURATION);

		public void Sleep(int DURATION);
		
		public int GetCurrentProcess();
	}
}
